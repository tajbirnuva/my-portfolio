import { Component, HostListener, OnInit } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { Location, PopStateEvent } from '@angular/common';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  public downloadCv =
    'https://drive.google.com/file/d/1yb0ERrczY2J0JGLL6LKgPg7qwmzRVoZs/view?usp=sharing';

  public isCollapsed = true;
  public screenWidth: any;

  constructor(public location: Location, private router: Router) {}

  ngOnInit() {
    this.onWindowResize();
  }

  @HostListener('window:resize', ['$event'])
  onWindowResize() {
    this.screenWidth = window.innerWidth;
  }
}
