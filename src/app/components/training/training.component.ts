import { Component, OnInit } from '@angular/core';
import { auto } from '@popperjs/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.scss'],
})
export class TrainingComponent implements OnInit {
  focus: any;
  focus1: any;
  constructor() {}

  ngOnInit(): void {}

  public show(imageUrl: string) {
    Swal.fire({
      width: 850,
      imageWidth: 800,
      imageHeight: 550,
      imageUrl: imageUrl,
      imageAlt: 'Certificate Image',
      showConfirmButton: false,
    });
  }
}
