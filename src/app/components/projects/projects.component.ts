import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface Details {
  title: string;
  description: string;
  module: string;
  backend: string;
  frontend: string;
  database: string;
}

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss'],
})
export class ProjectsComponent implements OnInit {
  
  projectList: Details[] = [
    {
      title: 'CRVS - Southtech Government Project',
      description:
        'CRVS is a microservice project that aims to produce legal information related to government educational institutes. The project includes modules such as educational information, student results, promotions, attendance, transfers, and stipends etc. The backend of the project uses Spring Boot, while the frontend is built on the Angular framework. Throughout this project I play a major role in the backend and also a supporting role in the frontend. My area of responsibility mainly relates to the Result, Stipend, and SMS-Email modules.',
      module: 'Crvs',
      backend: 'Spring Boot',
      frontend: 'Angular',
      database: 'MSSQL',
    },
    {
      title: 'AFS Banking System - Southtech Project',
      description: 'A banking project of Southtech Group, based on Microservice architecture, uses Spring Boot in the backend and Angular framework in the frontend. In this project, I have been involved with the Report and SMS-Email Module on the backend side.',
      module: 'Crvs',
      backend: 'Spring Boot',
      frontend: 'Angular Js',
      database: 'MySQL',
    },
    {
      title: 'Exam Portal System',
      description: 'The project is designed to simplify the process of conducting exams using Spring Boot and Angular framework and MySQL database. It is a user-friendly platform that allows students to register and take online exams. It also allows administrators to create and manage exam schedules, monitor progress & generate reports, and evaluate students based on their performance.',
      module: 'Crvs',
      backend: 'Spring Boot',
      frontend: 'Angular',
      database: 'MySQL',
    },
    {
      title: 'Smart Medical Registry System',
      description: 'The Smart Medical Registry System simplifies medical record-keeping by securely storing and managing patient medical records. It also facilitates the management of doctor serials and medical test details, including prerequisites, cost, and timing. The system provides patients with SMS and email notifications when the reports of their medical tests are available. Spring Boot is used for the application backend, Angular for its user interface, and MySQL for its database.',
      module: 'Crvs',
      backend: 'Spring Boot',
      frontend: 'Angular',
      database: 'MySQL',
    }, 
    
  {
      title: 'Supply Chain Management System - PONDIT Project',
      description: 'The Supply Chain Management System is a project designed to help manage the flow of goods and services, from the acquisition of raw materials to the delivery of finished products to customers. The backend of the project is built on Spring Boot and the system UI is designed using Thymeleaf. The project provides a range of features such as inventory management, logistics, procurement, and vendor management, which can help streamline the entire supply chain process.',
      module: 'Crvs',
      backend: 'Spring Boot',
      frontend: 'Thymeleaf',
      database: 'MySQL',
    },
  ];

  constructor(public matDialog: MatDialog) {}

  ngOnInit(): void {}

  showProjectDetails(i:number) {
    let project = this.projectList[i];
    this.matDialog.open(ProjectDetails, {
      data: {
        title: project.title,
        description: project.description,
        module: 'Crvs',
        backend: project.backend,
        frontend: project.frontend,
        database: project.database,
      },
    });
  }
}
@Component({
  selector: 'project-details',
  templateUrl: 'project-details.html',
})
export class ProjectDetails {
  constructor(@Inject(MAT_DIALOG_DATA) public project: Details) {}
}
